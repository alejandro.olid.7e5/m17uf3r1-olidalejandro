using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitDoor : MonoBehaviour
{
    public GameObject GameOverCanvas;
    public GameObject FindKeysCanvas;

    private void OnTriggerEnter(Collider collision)
    {
        PlayerController player = collision.gameObject.GetComponent<PlayerController>();

            if (player.inventorySO.KeysCollected.Count == 4)
            {
                GameOverCanvas.SetActive(true);
            }
            else
            {
                 FindKeysCanvas.SetActive(true);
             }
    }

    private void OnTriggerExit(Collider collision)
    {
        FindKeysCanvas.SetActive(false);

    }
}
