using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private CharacterController characterController;

    [SerializeField]
    public int ActualVelocity;

    [SerializeField]
    private int defaultVelocity = 1;

    [SerializeField]
    private int runningVelocity = 7;


    [SerializeField]
    private int stopVelocity = 0;

    [SerializeField]
    private bool isBored;

    [SerializeField]
    private float TimeWaiting;

    [SerializeField]
    private float TimeMaxForWait = 5;

    [SerializeField]
    private PlayerInput playerInput;

    [SerializeField]
    private Animator animator;

    public InventorySO inventorySO;

    private Vector2 playerMovement2D;

    private Vector3 playerMovement3D;


    void Start()
    {
        characterController = GetComponent<CharacterController>();
        playerInput = GetComponent<PlayerInput>();
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        animator.SetFloat("Velocity", ActualVelocity);
        playerMovement2D = playerInput.actions["Move"].ReadValue<Vector2>();
        if (playerMovement2D != null)
        {
            playerMovement3D.z = playerMovement2D.y;
            playerMovement3D.x = playerMovement2D.x;
            animator.SetFloat("Horizontal", playerMovement3D.x);
            animator.SetFloat("Vertical", playerMovement3D.z);

            characterController.Move(playerMovement3D * ActualVelocity * Time.deltaTime);
        }

        if (playerInput.actions["TakeItem"].WasPressedThisFrame())
        {
            animator.SetBool("IsTakingItem", true);
        }
        else
        {
            animator.SetBool("IsTakingItem", false);
        }

        if (playerInput.actions["Celebrate"].WasPressedThisFrame())
        {
            animator.SetBool("IsCelebrating", true);
        }
        else
        {
            animator.SetBool("IsCelebrating", false);
        }

        if (playerInput.actions["Jump"].WasPressedThisFrame())
        {
            animator.SetBool("IsJumping", true);
        }
        else
        {
            animator.SetBool("IsJumping", false);
        }

        if (playerInput.actions["Attack"].IsPressed())
        {
            animator.SetBool("IsPreparingToAttack", true);
            ActualVelocity = 0;
        }
        else
        {
            animator.SetBool("IsPreparingToAttack", false);
            ActualVelocity = defaultVelocity;
        }

        if (playerInput.actions["Dash"].WasPressedThisFrame())
        {
            animator.SetBool("IsDashing", true);

        }
        else
        {
            animator.SetBool("IsDashing", false);
        }

        if (playerInput.actions["Run"].IsPressed())
        {
            ActualVelocity = runningVelocity;
        }
        else
        {
            ActualVelocity = defaultVelocity;
        }

        if (playerInput.actions["Stand"].IsPressed())
        {
            animator.SetBool("IsStanding", true);
        }
        else
        {
            animator.SetBool("IsStanding", false);
        }



        if (isBored)
        {
            animator.SetBool("IsBored", true);
        }
        else
        {
            animator.SetBool("IsBored", false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        IPickable pickeable = other.gameObject.GetComponent<IPickable>();
        if (pickeable != null)
        {
            if (pickeable.IAmTheKey())
                inventorySO.KeysCollected.Add(true);
            Destroy(other.gameObject);
        }
    }
}
