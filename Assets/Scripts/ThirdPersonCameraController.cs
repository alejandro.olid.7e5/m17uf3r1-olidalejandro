
using UnityEngine;
using Cinemachine;

public class ThirdPersonCameraController : MonoBehaviour
{
    public CinemachineFreeLook cinemachineCamera;

    void Start()
    {
        cinemachineCamera.m_Lens.FieldOfView = 20;

    }

    void Update()
    {

    }
}
